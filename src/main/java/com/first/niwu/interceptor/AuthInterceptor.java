package com.first.niwu.interceptor;


import com.first.niwu.util.RedisUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 自定义拦截器
 * Created by Yangjin on 2018-09-30.
 */
public class AuthInterceptor extends HandlerInterceptorAdapter {

    private static Logger logger = LogManager.getLogger(AuthInterceptor.class.getName());
    public static final String[] API_ARR={"/app/login","/index/login","/images","/swagger-ui.html"};                                //可以直接通过token检测的接口

    @Autowired
    private RedisUtil redisUtil;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//
//        String requestURI = request.getRequestURI();
//        String tokenStr = request.getParameter("token");
//        //过滤指定接口，如login等
//        Boolean isPassAPI=false;
//        for(int i=0;i<API_ARR.length;i++){
//            if(requestURI.contains(API_ARR[i])){
//                isPassAPI=true;
//                break;
//            }
//        }
//
//        if(isPassAPI){
//            return true;
//        }else {
//            if(tokenStr == null){
//                String res = "{\"success\":false,\"data\":null,\"msg\":\"缺少token，无法验证\",\"code\":1001}";
//                dealErrorReturn( response, res);
//                return false;
//            }else{
//                String reqToken=null;
//                try{
//                    reqToken=redisUtil.get(tokenStr).toString();
//                }catch (Exception e){}
//                if (reqToken!= null) {
//                    return true;
//                }else{
//                    String res = "{\"success\":false,\"data\":null,\"msg\":\"当前登录用户失效，请重新登录\",\"code\":1002}";
//                    dealErrorReturn(response, res);
//                    return false;
//                }
//            }
//        }
//    }
//
//    // 检测到没有token，直接返回不验证
//    public void dealErrorReturn( HttpServletResponse httpServletResponse, Object obj) {
//        String json = (String) obj;
//        PrintWriter writer = null;
//        httpServletResponse.setCharacterEncoding("UTF-8");
//        httpServletResponse.setContentType("text/html; charset=utf-8");
//        try {
//            writer = httpServletResponse.getWriter();
//            writer.print(json);
//        } catch (IOException ex) {
//            logger.error(ex);
//        } finally {
//            if (writer != null)
//                writer.close();
//        }

 return  true;

   }
}
