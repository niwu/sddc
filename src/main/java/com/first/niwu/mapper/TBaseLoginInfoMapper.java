package com.first.niwu.mapper;

import com.first.niwu.model.TBaseLoginInfo;
import com.first.niwu.util.MyMapper;

public interface TBaseLoginInfoMapper extends MyMapper<TBaseLoginInfo> {
}