package com.first.niwu.mapper;

import com.first.niwu.model.TBaseModule;
import com.first.niwu.util.MyMapper;

public interface TBaseModuleMapper extends MyMapper<TBaseModule> {
}