package com.first.niwu.mapper;

import com.first.niwu.model.TRoleModule;
import com.first.niwu.util.MyMapper;

public interface TRoleModuleMapper extends MyMapper<TRoleModule> {
}