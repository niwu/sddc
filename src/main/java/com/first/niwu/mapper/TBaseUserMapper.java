package com.first.niwu.mapper;


import com.first.niwu.model.TBaseUser;
import com.first.niwu.util.MyMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface TBaseUserMapper extends MyMapper<TBaseUser> {
    //     根据loginName查询用户是否存在
    @Select("SELECT id, user_name userName, password, unit_id unitId, role_id roleId, login_times loginTimes FROM t_base_user\n" +
            "WHERE effective_state = 1 AND user_status = 1 AND login_name = #{loginName}")
    Map<String, Object> queryUserByLoginName(String loginName);

    //      根据id查询用户
    @Select("SELECT * FROM t_base_user WHERE effective_state = 1 AND user_status = 1 AND id = #{userId}")
    TBaseUser queryUserByUserId(Integer userId);

    //    根据主键id查询用户
    @Select("SELECT * FROM t_base_user WHERE effective_state = 1 AND user_status = 1 AND id = #{userId}")
    TBaseUser queryUserById(Integer id);

    //        根据id查询角色一级模块权限
    @Select("SELECT\n" +
            "\ttbm.id moduleId,\n" +
            "\ttbm.module_name moduleName,\n" +
            "  tbm.url\n" +
            "FROM\n" +
            "\tt_role_module trm,\n" +
            "\tt_base_module tbm\n" +
            "WHERE\n" +
            "\ttrm.module_id = tbm.id\n" +
            "AND\n" +
            "tbm.effective_state =1\n" +
            "AND\n" +
            "tbm.module_status =1\n" +
            "AND tbm.parent_id = 0\n" +
            "AND trm.role_id = #{roleId}")
    List<Map<String, Object>> queryModuleByRoleId(Integer roleId);

    @Select("\n" +
            "SELECT\n" +
            "\ttbm.id moduleId,\n" +
            "\ttbm.module_name moduleName,\n" +
            "\ttbm.url\n" +
            "FROM\n" +
            "\tt_role_module trm,\n" +
            "\tt_base_module tbm\n" +
            "WHERE\n" +
            "\ttrm.module_id = tbm.id\n" +
            "AND tbm.effective_state = 1\n" +
            "AND tbm.module_status = 1\n" +
            "AND trm.role_id = #{roleId} and tbm.parent_id = #{parentId}")
   List<Map<String ,Object>> queryModuleByRoleIdAndParentId(@Param("roleId") Integer roleId, @Param("parentId") Integer parentId);

     @Select("SELECT * FROM t_base_user WHERE effective_state = 1 AND email = #{email}")
    TBaseUser queryUserByEmial(String email);

     @Select("SELECT * FROM t_base_user WHERE effective_state = 1 AND email = #{email}")
    TBaseUser queryUserByEmail(String email);
}

