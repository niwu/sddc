package com.first.niwu.util;

import java.lang.annotation.*;

/**
 * Created by RBWH_JS on 2019/3/12.
 * zhenwgei
 */
@Target({ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface CacheParam {

    /**
     * 字段名称
     *
     * @return String
     */
    String name() default "";
}
