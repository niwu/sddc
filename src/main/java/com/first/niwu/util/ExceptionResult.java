package com.first.niwu.util;

import com.first.niwu.enums.ExceptionEnum;
import lombok.Data;

/**
 * Created by RBWH_JS on 2019/1/21.
 * zhenwgei
 */
@Data
public class ExceptionResult {

    private int status;
    private String message;
    private Long timestamp;

    public ExceptionResult(ExceptionEnum em) {
        this.status = em.getCode();
        this.message = em.getMsg();
        this.timestamp = System.currentTimeMillis();
    }
}
