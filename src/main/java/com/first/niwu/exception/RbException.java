package com.first.niwu.exception;

import com.first.niwu.enums.ExceptionEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by RBWH_JS on 2019/1/21.
 * zhenwgei
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class RbException extends  RuntimeException{

    private ExceptionEnum exceptionEnum;

}
