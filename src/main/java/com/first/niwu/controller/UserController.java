package com.first.niwu.controller;

import com.first.niwu.model.TBaseUser;
import com.first.niwu.service.UserService;
import com.first.niwu.util.CacheLock;
import com.first.niwu.util.CacheParam;
import com.first.niwu.util.PageResult;
import io.swagger.annotations.*;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Author: 倪五
 * @Date：2019/6/29 8:27
 */
@RestController
@RequestMapping(value = "/user")
@Api(description = "登录接口")
public class UserController {
    //   注入UserService方法
    @Autowired
    private UserService userService;

    @PostMapping(value = "/login")
    @ApiOperation(value = "/用户登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "loginName", value = "登录名", paramType = "query", required = true, dataType = "String"),
            @ApiImplicitParam(name = "password", value = "密码", paramType = "query", required = true, dataType = "String")
    })
    public ResponseEntity<Map<String, Object>> login(HttpServletRequest request, String loginName, String password) {
        return ResponseEntity.ok(userService.queryByLoginNameAndPassword(request, loginName, password));
    }

    @PostMapping(value = "/save")
    @ApiOperation(value = "新增用户")
    public ResponseEntity saveUser(@Validated @RequestBody @ApiParam(name = "用户对象", value = "传入json格式", required = true) TBaseUser user) {
        userService.saveUser(user);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping(value = "/deleteUser")
//   防止重复提交
    @CacheLock(prefix = "user:arg[0]")
    @ApiOperation(value = "删除用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户id", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "token", value = "token值", paramType = "query", dataType = "String")
    })
    public ResponseEntity deleteUser(@CacheParam(name = "id") Integer id, @CacheParam(name = "token") String token) {
//      传入 id  token
        userService.deleteUser(id, token);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/updateUser")
    @ApiOperation(value = "更改用户")
    public ResponseEntity updateUser(@Validated @RequestBody @ApiParam(name = "用户对象", value = "传入json格式", required = true) TBaseUser user) {
        userService.updateUser(user);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @ApiOperation(value = "查询用户列表")
    @GetMapping(value = "/queryUserList")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyWord", value = "关键词", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "roleId", value = "角色id", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "userStatus", value = "状态是否启用", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "page", value = "当前页", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "rows", value = "每页显示数目", paramType = "query", required = true, dataType = "int"),
    })
    public ResponseEntity<PageResult<TBaseUser>> queryUserList(String keyWord, Integer roleId, Integer userStatus, Integer page, Integer rows) {
        return ResponseEntity.ok(userService.queryUserList(keyWord, roleId, userStatus, page, rows));
    }
    @GetMapping(value = "/loginOut")
    @ApiOperation("安全退出")
    @ApiImplicitParam(name = "token" , value = "token值" ,required = true, paramType = "query", dataType = "token")
    public ResponseEntity loginOut(String token) {
        userService.loginOut(token);
        return ResponseEntity.ok().build();
    }
    @PostMapping(value = "/updatePassword")
//    防止重复提交
    @CacheLock(prefix = "password:arg[0]")
    @ApiOperation(value = "修改密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token值", paramType = "query", required = true, dataType = "String"),
            @ApiImplicitParam(name = "oldPassword", value ="旧密码", paramType = "query", required = true, dataType = "String"),
            @ApiImplicitParam(name = "newPassword", value = "新密码", paramType = "query", required = true, dataType = "String")
    })

  public ResponseEntity updatePassword(String token, String oldPassword, String newPassword){
        userService.updatePassword(token, oldPassword, newPassword);
        return ResponseEntity.ok().build();
  }


}
