package com.first.niwu.controller;

import com.first.niwu.model.TBaseModule;
import com.first.niwu.service.ModuleService;
import com.first.niwu.util.CacheLock;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: 倪五
 * @Date：2019/7/2 21:12
 */
@RestController
@RequestMapping("/module")
@Api(description = "模块管理")
public class ModuleController {
    @Autowired
    private ModuleService moduleService;
    @PostMapping(value = "/saveModule")
//     防止重复提交
    @CacheLock(prefix = "user:arg[0]")
    @ApiOperation(value = "新增模块")
    public ResponseEntity saveModule(@Validated @RequestBody @ApiParam(name = "用户对象", value = "传入json格式", required = true) TBaseModule module) {
        moduleService.saveModule(module);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }


}
