package com.first.niwu.controller;

import lombok.var;

import java.util.regex.Pattern;

/**
 * @Author: 倪五
 * @Date：2019/7/2 8:39
 */
public class RegularExpression {
    public static void main(String[] args){
        String content = "I am noob " +
                "from runoob.com.";

        String pattern = ".*runoob.*";

        boolean isMatch = Pattern.matches(pattern, content);
        System.out.println("字符串中是否包含了 'runoob' 子字符串? " + isMatch);
        var str = "Is is the cost of of gasoline going up up";

    }
}
