package com.first.niwu;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;


@SpringBootApplication
@MapperScan(value = "com.first.niwu.mapper")
@EnableSwagger2
public class NiwuApplication {

    public static void main(String[] args) {
        SpringApplication.run(NiwuApplication.class, args);
    }

}
