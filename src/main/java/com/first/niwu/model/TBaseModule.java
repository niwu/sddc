package com.first.niwu.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import javax.persistence.*;
@ApiModel(value = "modul对象", description = "模块对象modul")
@Table(name = "t_base_module")
public class TBaseModule {
    /**
     * 模块表主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "模块主键id")
    private Integer id;

    /**
     * 模块名称
     */
    @Column(name = "module_name")
    @ApiModelProperty(value ="模块名称", required = true)
    private String moduleName;

    /**
     * 模块图标 id 关联字典表id
     */
    @ApiModelProperty(value = "模块图标", required = true)
    private Integer   icon;

    /**
     * 模块的url地址
     */
    @ApiModelProperty(value = "模块url地址", required = true)
    private String   url;

    /**
     * 模块父节点id
     */
    @ApiModelProperty(value = "模块父节点id")
    @Column(name = "parent_id")
    private Integer parentId;

    /**
     * 模块备注信息（主要针对主页）
     */
    @ApiModelProperty(value = "模块备注信息（主要针对主页）")
    private String remark;

    /**
     * 是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     */
    @Column(name = "effective_state")
    @ApiModelProperty(value = "是否有效 （1为有效（刚创建时）0为逻辑删除页面不显示）", required = true)
    private Integer effectiveState;

    /**
     * 模块状态（0 未启用 1启用 ）
     */
    @ApiModelProperty(value = "模块状态（1启用，0未启用）", required = true)
    @Column(name = "module_status")
    private Integer moduleStatus;

    /**
     * 创建人
     */
    @ApiModelProperty(value="创建人" , hidden = true)
    @Column(name = "create_user_id")
    private Integer createUserId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", hidden = true)
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改用户id信息
     */
    @ApiModelProperty(value = "修改用户id信息", hidden = true)
    @Column(name = "modify_user_id")
    private Integer modifyUserId;

    /**
     * 最后一次修改时间
     */
    @ApiModelProperty(value = "最后一次修改时间", hidden = true)
    @Column(name = "modify_time")
    private Date modifyTime;

    /**
     * 获取模块表主键id
     *
     * @return id - 模块表主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置模块表主键id
     *
     * @param id 模块表主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取模块名称
     *
     * @return module_name - 模块名称
     */
    public String getModuleName() {
        return moduleName;
    }

    /**
     * 设置模块名称
     *
     * @param moduleName 模块名称
     */
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    /**
     * 获取模块图标 id 关联字典表id
     *
     * @return icon - 模块图标 id 关联字典表id
     */
    public Integer getIcon() {
        return icon;
    }

    /**
     * 设置模块图标 id 关联字典表id
     *
     * @param icon 模块图标 id 关联字典表id
     */
    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    /**
     * 获取模块的url地址
     *
     * @return url - 模块的url地址
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置模块的url地址
     *
     * @param url 模块的url地址
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 获取模块父节点id
     *
     * @return parent_id - 模块父节点id
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * 设置模块父节点id
     *
     * @param parentId 模块父节点id
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取模块备注信息（主要针对主页）
     *
     * @return remark - 模块备注信息（主要针对主页）
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置模块备注信息（主要针对主页）
     *
     * @param remark 模块备注信息（主要针对主页）
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     *
     * @return effective_state - 是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     */
    public Integer getEffectiveState() {
        return effectiveState;
    }

    /**
     * 设置是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     *
     * @param effectiveState 是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     */
    public void setEffectiveState(Integer effectiveState) {
        this.effectiveState = effectiveState;
    }

    /**
     * 获取模块状态（0 未启用 1启用 ）
     *
     * @return module_status - 模块状态（0 未启用 1启用 ）
     */
    public Integer getModuleStatus() {
        return moduleStatus;
    }

    /**
     * 设置模块状态（0 未启用 1启用 ）
     *
     * @param moduleStatus 模块状态（0 未启用 1启用 ）
     */
    public void setModuleStatus(Integer moduleStatus) {
        this.moduleStatus = moduleStatus;
    }

    /**
     * 获取创建人
     *
     * @return create_user_id - 创建人
     */
    public Integer getCreateUserId() {
        return createUserId;
    }

    /**
     * 设置创建人
     *
     * @param createUserId 创建人
     */
    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改用户id信息
     *
     * @return modify_user_id - 修改用户id信息
     */
    public Integer getModifyUserId() {
        return modifyUserId;
    }

    /**
     * 设置修改用户id信息
     *
     * @param modifyUserId 修改用户id信息
     */
    public void setModifyUserId(Integer modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    /**
     * 获取最后一次修改时间
     *
     * @return modify_time - 最后一次修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置最后一次修改时间
     *
     * @param modifyTime 最后一次修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}