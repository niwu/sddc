package com.first.niwu.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Table(name = "t_base_user")
@ApiModel(value = "user对象", description = "用户对象user")
public class TBaseUser {
    @Transient
    private String token;

    public String getToken() {
        return token;
    }

    /**
     * 用户表主键
     */
    @Id
    @ApiModelProperty(value = "用户名", name = "userId", dataType = "int")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名", name = "userName", dataType = "String", required = true)
    @Column(name = "user_name")
    private String userName;

    /**
     * 用户登录名
     */
    @ApiModelProperty(value = "登录名", name = "loginName", required = true, dataType = "String")
    @Column(name = "login_name")
    private String loginName;

    /**
     * 用户密码
     */
    @ApiModelProperty(value = "密码", name = "password", hidden = true, dataType = "String ")
    private String password;

    /**
     * 所属单位id，关联单位表
     */
    @ApiModelProperty(value = "所属单位id", name = "unitId", required = true, dataType = "int")
    @Column(name = "unit_id")
    private Integer unitId;

    /**
     * 用户手机号码
     */
    @NotBlank(message = "手机号不能为空")
    @Pattern(regexp = "^1[0-9]{10}$",message = "手机号格式错误")
    @ApiModelProperty(value = "手机号码", name = "mobile", required = true, dataType = "String")
    private String mobile;

    /**
     * 用户微信号
     */
    @ApiModelProperty(value = "微信号", name = "weChat", required = true, dataType = "String")
    @Column(name = "we_chat")
    private String weChat;

    /**
     * 用户邮箱
     */
    @NotBlank(message = "邮箱不能为u空")
    @Email(regexp = "^\\w[-\\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\\.)+[A-Za-z]{2,14}$", message = "邮箱格式错误")
    @ApiModelProperty(value = "邮箱", name = "emial", required = true, dataType = "String")
    private String email;

    /**
     * QQ号
     */

    @ApiModelProperty(value = "qq号", name = "qq", required = true, dataType = "String")
    private String qq;

    /**
     * 登录次数
     */
    @ApiModelProperty(value = "登录次数", name = "loginName", hidden = true, dataType = "int")
    @Column(name = "login_times")
    private Integer loginTimes;

    /**
     * 用户状态 （0 未启用 1启用 ）
     */
    @ApiModelProperty(value = "是否启用(0 未启用， 1 启用）", name = "userStatus", required = true, dataType = "int")
    @Column(name = "user_status")
    private Integer userStatus;

    /**
     * 是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     */
    @Column(name = "effective_state")
    @ApiModelProperty(value = "是否有效（1 有效， 0逻辑删除页面不显示)", name = "effectiveState", dataType = "int", hidden = true)
    private Integer effectiveState;

    /**
     * 用户角色ID（关联角色表id）
     */
    @Column(name = "role_id")
    @ApiModelProperty(value = "角色id", name = "roleId", dataType = "int", required = true)
    private Integer roleId;

    private String remark;

    /**
     * 最后一次登录时间
     */
    @ApiModelProperty(value = "最后一次登录时间", name = "lastLoginName", dataType = "Date", hidden = true)
    @Column(name = "last_login_time")
    private Date lastLoginTime;

    /**
     * 最后一次登录mac地址
     */
    @ApiModelProperty(value = "最后一次登录mac地址", name = "lastLoginMac", dataType = "String", hidden = true)
    @Column(name = "last_login_mac")
    private String lastLoginMac;

    /**
     * 最后一次登录IP地址
     */
    @ApiModelProperty(value = "最后一次登录ip地址", name = "lastLoginIp", dataType = "String", hidden = true)
    @Column(name = "last_login_ip")
    private String lastLoginIp;

    /**
     * 创建用户id 关联t_base_user表id
     */
    @ApiModelProperty(value = "与用户id关联的t_base_user表的id", name = "crerteUserId", dataType = "int", hidden = true)
    @Column(name = "create_user_id")
    private Integer createUserId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", name = "createTime", dataType = "Date", hidden = true)
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 最后一次修改用户id 关联t_base_user表id
     */
    @ApiModelProperty(value = "最后一次修改用户id 关联t_base_user表id", name = "modifyUserId", dataType = "int", hidden = true)
    @Column(name = "modify_user_id")
    private Integer modifyUserId;

    /**
     * 最后一次修改时间
     */
    @Column(name = "modify_time")
    @ApiModelProperty(value = "最后一次修改时间", name = "modifyTime", dataType = "Date", hidden = true)
    private Date modifyTime;

    /**
     * 获取用户表主键
     *
     * @return id - 用户表主键
     */

    public Integer getId() {
        return id;
    }

    /**
     * 设置用户表主键
     *
     * @param id 用户表主键
     */

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取用户名
     *
     * @return user_name - 用户名
     */
    public String getUserName() {

        return userName;
    }

    /**
     * 设置用户名
     *
     * @param userName 用户名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取用户登录名
     *
     * @return login_name - 用户登录名
     */
    public String getLoginName() {
        return loginName;
    }

    /**
     * 设置用户登录名
     *
     * @param loginName 用户登录名
     */
    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    /**
     * 获取用户密码
     *
     * @return password - 用户密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置用户密码
     *
     * @param password 用户密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取所属单位id，关联单位表
     *
     * @return unit_id - 所属单位id，关联单位表
     */
    public Integer getUnitId() {
        return unitId;
    }

    /**
     * 设置所属单位id，关联单位表
     *
     * @param unitId 所属单位id，关联单位表
     */
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    /**
     * 获取用户手机号码
     *
     * @return mobile - 用户手机号码
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置用户手机号码
     *
     * @param mobile 用户手机号码
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取用户微信号
     *
     * @return we_chat - 用户微信号
     */
    public String getWeChat() {
        return weChat;
    }

    /**
     * 设置用户微信号
     *
     * @param weChat 用户微信号
     */
    public void setWeChat(String weChat) {
        this.weChat = weChat;
    }

    /**
     * 获取用户邮箱
     *
     * @return email - 用户邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置用户邮箱
     *
     * @param email 用户邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取QQ号
     *
     * @return qq - QQ号
     */
    public String getQq() {
        return qq;
    }

    /**
     * 设置QQ号
     *
     * @param qq QQ号
     */
    public void setQq(String qq) {
        this.qq = qq;
    }

    /**
     * 获取登录次数
     *
     * @return login_times - 登录次数
     */
    public Integer getLoginTimes() {
        return loginTimes;
    }

    /**
     * 设置登录次数
     *
     * @param loginTimes 登录次数
     */
    public void setLoginTimes(Integer loginTimes) {
        this.loginTimes = loginTimes;
    }

    /**
     * 获取用户状态 （0 未启用 1启用 ）
     *
     * @return user_status - 用户状态 （0 未启用 1启用 ）
     */
    public Integer getUserStatus() {
        return userStatus;
    }

    /**
     * 设置用户状态 （0 未启用 1启用 ）
     *
     * @param userStatus 用户状态 （0 未启用 1启用 ）
     */
    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    /**
     * 获取是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     *
     * @return effective_state - 是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     */
    public Integer getEffectiveState() {
        return effectiveState;
    }

    /**
     * 设置是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     *
     * @param effectiveState 是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     */
    public void setEffectiveState(Integer effectiveState) {
        this.effectiveState = effectiveState;
    }

    /**
     * 获取用户角色ID（关联角色表id）
     *
     * @return role_id - 用户角色ID（关联角色表id）
     */
    public Integer getRoleId() {
        return roleId;
    }

    /**
     * 设置用户角色ID（关联角色表id）
     *
     * @param roleId 用户角色ID（关联角色表id）
     */
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取最后一次登录时间
     *
     * @return last_login_time - 最后一次登录时间
     */
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    /**
     * 设置最后一次登录时间
     *
     * @param lastLoginTime 最后一次登录时间
     */
    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    /**
     * 获取最后一次登录mac地址
     *
     * @return last_login_mac - 最后一次登录mac地址
     */
    public String getLastLoginMac() {
        return lastLoginMac;
    }

    /**
     * 设置最后一次登录mac地址
     *
     * @param lastLoginMac 最后一次登录mac地址
     */
    public void setLastLoginMac(String lastLoginMac) {
        this.lastLoginMac = lastLoginMac;
    }

    /**
     * 获取最后一次登录IP地址
     *
     * @return last_login_ip - 最后一次登录IP地址
     */
    public String getLastLoginIp() {
        return lastLoginIp;
    }

    /**
     * 设置最后一次登录IP地址
     *
     * @param lastLoginIp 最后一次登录IP地址
     */
    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    /**
     * 获取创建用户id 关联t_base_user表id
     *
     * @return create_user_id - 创建用户id 关联t_base_user表id
     */
    public Integer getCreateUserId() {
        return createUserId;
    }

    /**
     * 设置创建用户id 关联t_base_user表id
     *
     * @param createUserId 创建用户id 关联t_base_user表id
     */
    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取最后一次修改用户id 关联t_base_user表id
     *
     * @return modify_user_id - 最后一次修改用户id 关联t_base_user表id
     */
    public Integer getModifyUserId() {
        return modifyUserId;
    }

    /**
     * 设置最后一次修改用户id 关联t_base_user表id
     *
     * @param modifyUserId 最后一次修改用户id 关联t_base_user表id
     */
    public void setModifyUserId(Integer modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    /**
     * 获取最后一次修改时间
     *
     * @return modify_time - 最后一次修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置最后一次修改时间
     *
     * @param modifyTime 最后一次修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }


}