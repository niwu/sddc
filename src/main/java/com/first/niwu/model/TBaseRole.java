package com.first.niwu.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_base_role")
public class TBaseRole {
    /**
     * 角色表主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 角色名称
     */
    @Column(name = "role_name")
    private String roleName;

    /**
     * 角色描述信息
     */
    private String remark;

    /**
     * 角色状态(1 启用 0 未启用)
     */
    @Column(name = "role_status")
    private Integer roleStatus;

    /**
     * 是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     */
    @Column(name = "effective_state")
    private Integer effectiveState;

    /**
     * 最初创建时间
     */
    @Column(name = "create_user_id")
    private Integer createUserId;

    /**
     * 最初创建人员id
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 记录最后一次修改人员id
     */
    @Column(name = "modify_user_id")
    private Integer modifyUserId;

    /**
     * 记录最后一次修改时间
     */
    @Column(name = "modify_time")
    private Date modifyTime;

    /**
     * 获取角色表主键id
     *
     * @return id - 角色表主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置角色表主键id
     *
     * @param id 角色表主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取角色名称
     *
     * @return role_name - 角色名称
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * 设置角色名称
     *
     * @param roleName 角色名称
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * 获取角色描述信息
     *
     * @return remark - 角色描述信息
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置角色描述信息
     *
     * @param remark 角色描述信息
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取角色状态(1 启用 0 未启用)
     *
     * @return role_status - 角色状态(1 启用 0 未启用)
     */
    public Integer getRoleStatus() {
        return roleStatus;
    }

    /**
     * 设置角色状态(1 启用 0 未启用)
     *
     * @param roleStatus 角色状态(1 启用 0 未启用)
     */
    public void setRoleStatus(Integer roleStatus) {
        this.roleStatus = roleStatus;
    }

    /**
     * 获取是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     *
     * @return effective_state - 是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     */
    public Integer getEffectiveState() {
        return effectiveState;
    }

    /**
     * 设置是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     *
     * @param effectiveState 是否有效（1有效（刚创建的时候） ，0 逻辑删除 页面不显示）
     */
    public void setEffectiveState(Integer effectiveState) {
        this.effectiveState = effectiveState;
    }

    /**
     * 获取最初创建时间
     *
     * @return create_user_id - 最初创建时间
     */
    public Integer getCreateUserId() {
        return createUserId;
    }

    /**
     * 设置最初创建时间
     *
     * @param createUserId 最初创建时间
     */
    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

    /**
     * 获取最初创建人员id
     *
     * @return create_time - 最初创建人员id
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置最初创建人员id
     *
     * @param createTime 最初创建人员id
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取记录最后一次修改人员id
     *
     * @return modify_user_id - 记录最后一次修改人员id
     */
    public Integer getModifyUserId() {
        return modifyUserId;
    }

    /**
     * 设置记录最后一次修改人员id
     *
     * @param modifyUserId 记录最后一次修改人员id
     */
    public void setModifyUserId(Integer modifyUserId) {
        this.modifyUserId = modifyUserId;
    }

    /**
     * 获取记录最后一次修改时间
     *
     * @return modify_time - 记录最后一次修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置记录最后一次修改时间
     *
     * @param modifyTime 记录最后一次修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}