package com.first.niwu.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_base_login_info")
public class TBaseLoginInfo {
    /**
     * 日志主键表
     */
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 日志类型(主要保存1 用户登录、2 用户退出、3 信息删除、4 数据导出、5 异常 6 数据修改 7 重置密码 保存基本日志类型的存储)
日志类型提前定义好 1 
     */
    @Column(name = "log_type")
    private Integer logType;

    /**
     * 日志内容
     */
    @Column(name = "log_content")
    private String logContent;

    /**
     * 操作用户ID（不关联用户表，直接存储)
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 操作方法
     */
    @Column(name = "action_method")
    private String actionMethod;

    @Column(name = "user_name")
    private String userName;

    /**
     * 记录操作模块的id
     */
    @Column(name = "module_id")
    private Integer moduleId;

    /**
     * 记录操作主机IP
     */
    @Column(name = "operate_ip")
    private String operateIp;

    /**
     * 记录操作主机的mac地址
     */
    @Column(name = "operate_mac")
    private String operateMac;

    /**
     * 记录操作时间
     */
    @Column(name = "operate_time")
    private Date operateTime;

    /**
     * 状态内型 1成功，2失败
     */
    @Column(name = "status_type")
    private Integer statusType;

    /**
     * 获取日志主键表
     *
     * @return id - 日志主键表
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置日志主键表
     *
     * @param id 日志主键表
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取日志类型(主要保存1 用户登录、2 用户退出、3 信息删除、4 数据导出、5 异常 6 数据修改 7 重置密码 保存基本日志类型的存储)
日志类型提前定义好 1 
     *
     * @return log_type - 日志类型(主要保存1 用户登录、2 用户退出、3 信息删除、4 数据导出、5 异常 6 数据修改 7 重置密码 保存基本日志类型的存储)
日志类型提前定义好 1 
     */
    public Integer getLogType() {
        return logType;
    }

    /**
     * 设置日志类型(主要保存1 用户登录、2 用户退出、3 信息删除、4 数据导出、5 异常 6 数据修改 7 重置密码 保存基本日志类型的存储)
日志类型提前定义好 1 
     *
     * @param logType 日志类型(主要保存1 用户登录、2 用户退出、3 信息删除、4 数据导出、5 异常 6 数据修改 7 重置密码 保存基本日志类型的存储)
日志类型提前定义好 1 
     */
    public void setLogType(Integer logType) {
        this.logType = logType;
    }

    /**
     * 获取日志内容
     *
     * @return log_content - 日志内容
     */
    public String getLogContent() {
        return logContent;
    }

    /**
     * 设置日志内容
     *
     * @param logContent 日志内容
     */
    public void setLogContent(String logContent) {
        this.logContent = logContent;
    }

    /**
     * 获取操作用户ID（不关联用户表，直接存储)
     *
     * @return user_id - 操作用户ID（不关联用户表，直接存储)
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置操作用户ID（不关联用户表，直接存储)
     *
     * @param userId 操作用户ID（不关联用户表，直接存储)
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取操作方法
     *
     * @return action_method - 操作方法
     */
    public String getActionMethod() {
        return actionMethod;
    }

    /**
     * 设置操作方法
     *
     * @param actionMethod 操作方法
     */
    public void setActionMethod(String actionMethod) {
        this.actionMethod = actionMethod;
    }

    /**
     * @return user_name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取记录操作模块的id
     *
     * @return module_id - 记录操作模块的id
     */
    public Integer getModuleId() {
        return moduleId;
    }

    /**
     * 设置记录操作模块的id
     *
     * @param moduleId 记录操作模块的id
     */
    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    /**
     * 获取记录操作主机IP
     *
     * @return operate_ip - 记录操作主机IP
     */
    public String getOperateIp() {
        return operateIp;
    }

    /**
     * 设置记录操作主机IP
     *
     * @param operateIp 记录操作主机IP
     */
    public void setOperateIp(String operateIp) {
        this.operateIp = operateIp;
    }

    /**
     * 获取记录操作主机的mac地址
     *
     * @return operate_mac - 记录操作主机的mac地址
     */
    public String getOperateMac() {
        return operateMac;
    }

    /**
     * 设置记录操作主机的mac地址
     *
     * @param operateMac 记录操作主机的mac地址
     */
    public void setOperateMac(String operateMac) {
        this.operateMac = operateMac;
    }

    /**
     * 获取记录操作时间
     *
     * @return operate_time - 记录操作时间
     */
    public Date getOperateTime() {
        return operateTime;
    }

    /**
     * 设置记录操作时间
     *
     * @param operateTime 记录操作时间
     */
    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }

    /**
     * 获取状态内型 1成功，2失败
     *
     * @return status_type - 状态内型 1成功，2失败
     */
    public Integer getStatusType() {
        return statusType;
    }

    /**
     * 设置状态内型 1成功，2失败
     *
     * @param statusType 状态内型 1成功，2失败
     */
    public void setStatusType(Integer statusType) {
        this.statusType = statusType;
    }
}