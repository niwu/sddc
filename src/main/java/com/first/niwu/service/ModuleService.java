package com.first.niwu.service;

import com.first.niwu.model.TBaseModule;

/**
 * @Author: 倪五
 * @Date：2019/7/2 21:17
 */
public interface ModuleService {
    void saveModule(TBaseModule module);
}
