package com.first.niwu.service;

import com.first.niwu.model.TBaseUser;
import com.first.niwu.util.PageResult;

import javax.naming.ldap.PagedResultsControl;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Author: 倪五
 * @Date：2019/6/29 8:29
 */
public interface UserService {


    Map<String, Object> queryByLoginNameAndPassword(HttpServletRequest request, String loginName, String password);

    void saveUser(TBaseUser user);

    void deleteUser(Integer id, String token);

    void updateUser(TBaseUser user);

    void loginOut(String token);

   PageResult<TBaseUser> queryUserList(String keyWord, Integer roleId, Integer userStatus, Integer page, Integer rows);

    void updatePassword(String token, String oldPassword, String newPassword);
}
