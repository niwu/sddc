package com.first.niwu.service.Impl;

import com.first.niwu.enums.ExceptionEnum;
import com.first.niwu.exception.RbException;


import com.first.niwu.mapper.TBaseLoginInfoMapper;
import com.first.niwu.mapper.TBaseUserMapper;

import com.first.niwu.model.TBaseLoginInfo;
import com.first.niwu.model.TBaseUser;
import com.first.niwu.service.UserService;


import com.first.niwu.util.JsonUtil;
import com.first.niwu.util.MacUtil;
import com.first.niwu.util.PageResult;
import com.first.niwu.util.RedisUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import javax.servlet.http.HttpServletRequest;
import java.beans.Transient;
import java.util.*;

/**
 * @Author: 倪五
 * @Date：2019/6/29 8:42
 * 登录接口
 */
@Service
public class UserServiceImpl implements UserService {
    //    注入TBaseUserMapper 方法
    @Autowired
    private TBaseUserMapper tBaseUserMapper;
    @Autowired
    private TBaseLoginInfoMapper tBaseLoginInfoMapper;
    @Autowired
    private RedisUtil redisUtil;

    private static Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> queryByLoginNameAndPassword(HttpServletRequest request, String loginName, String password) {
        String ip = "";
        String mac = "";
        try {
//          获取ip地址
            ip = MacUtil.getIp(request);
            mac = MacUtil.getLocalMac(ip);
        } catch (Exception e) {
            e.printStackTrace();
        }
//      将登录记录存到tBaseLoginInfo表中
        TBaseLoginInfo tBaseLoginInfo = new TBaseLoginInfo();
        tBaseLoginInfo.setOperateMac(mac);
        tBaseLoginInfo.setOperateTime(new Date());
        tBaseLoginInfo.setLogType(2);
        tBaseLoginInfo.setOperateIp(ip);
        tBaseLoginInfo.setActionMethod(request.getServletPath());
        Map<String, Object> userMap = tBaseUserMapper.queryUserByLoginName(loginName);
//      判断当前登录名是否存在
        if (userMap == null) {
            log.error("登录失败，用户名不存在");
//            抛出异常   用户名错误
            throw new RbException(ExceptionEnum.NOT_USER_NAME);
        }
//        判断密码是否正确
        String pwd = "password";
        if (!userMap.get(pwd).toString().equals(password)) {
            log.error("登录失败，密码错误");
//            抛出异常 密码错误
            throw new RbException(ExceptionEnum.NOT_USER_PWD);
        }
//        判断用户是否有角色
        if (userMap.get("roleId") == null) {
//            抛出异常 当前账户未分配角色
            throw new RbException(ExceptionEnum.NOT_USER_ROLE);
        }
//        移除密码
        userMap.remove("password");
        tBaseLoginInfo.setLogContent(ExceptionEnum.LOGIN_SUCCESS.getMsg());
//       更新tBaseLoginInfo表中信息
        tBaseLoginInfo.setStatusType(1);
        tBaseLoginInfo.setUserId(Integer.valueOf(userMap.get("id").toString()));
        tBaseLoginInfo.setUserName(userMap.get("userName").toString());
        tBaseLoginInfoMapper.insert(tBaseLoginInfo);
//        更新tBaseUser表中的信息
        TBaseUser tBaseUser = tBaseUserMapper.queryUserById(Integer.valueOf(userMap.get("id").toString()));
        tBaseUser.setLoginTimes(Integer.valueOf(userMap.get("loginTimes").toString()) + 1);
        tBaseUser.setLastLoginIp(ip);
        tBaseUser.setLastLoginMac(mac);
        tBaseUser.setLastLoginTime(new Date());
        tBaseUser.setUserName(userMap.get("userName").toString());
//        将用户信息存入redis中
        String token = UUID.randomUUID().toString();
        redisUtil.set(token, JsonUtil.object2Json(userMap));
//        更新用户登录信息
        tBaseUserMapper.updateByPrimaryKeySelective(tBaseUser);
//        返回当前拥有的权限
        List<Map<String, Object>> list = tBaseUserMapper.queryModuleByRoleId(Integer.valueOf(userMap.get("roleId").toString()));
        if (!CollectionUtils.isEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
//               获取二级菜单
                list.get(i).put("module", tBaseUserMapper.queryModuleByRoleIdAndParentId(Integer.valueOf(userMap.get("roleId").toString()), Integer.valueOf(list.get(i).get("moduleId").toString())));
            }

        }
        userMap.put("modules", list);
        userMap.put("token", token);
        log.info("登录成功");

        return userMap;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveUser(TBaseUser user) {
//        创建新对象user1
        TBaseUser user1 = new TBaseUser();

        try {
            user1 = JsonUtil.json2Object(redisUtil.get(user.getToken()).toString(), TBaseUser.class);
        } catch (Exception e) {
            log.error("json解析异常", e);
        }
//        判断 是否在登录状态
        if (user1.getId() == null) {
            log.error("用户未登录");
//            抛出  账户未登录  请登陆后再操作
            throw new RbException(ExceptionEnum.NOT_LOGIN);
        }
//        判断是否有同名的
        if (tBaseUserMapper.queryUserByLoginName(user.getLoginName()) != null) {
////        抛出  该用户名已经存在
            throw new RbException(ExceptionEnum.COMMON_USER_NAME);
        }
//        判断邮箱是否已经存在
        if (tBaseUserMapper.queryUserByEmial(user.getEmail()) != null) {
//            抛出异常  当前邮箱已经被注册
            throw new RbException(ExceptionEnum.COMMON_USER_EMAIL);
        }
//        添加用户的其他属性（EffectiveState，CreateTime，CreateUserId，ModifyUserId，Password）
        user.setEffectiveState(1);
        user.setCreateTime(new Date());
        user.setCreateUserId(user1.getId());
        user.setModifyUserId(user1.getId());
        user.setLoginTimes(0);
        user.setPassword("123456");
        tBaseUserMapper.insertUseGeneratedKeys(user);
//        判断返回的主键id是否为空
        if (user.getId() == null) {
            log.error("增加用户失败");
//            抛出   新增用户失败
            throw new RbException(ExceptionEnum.SAVE_USER_ERROR);
        }
    }

    /**
     * 通过id和token删除用户
     *
     * @param id
     * @param token
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteUser(Integer id, String token) {
//        创建新对象user2
        TBaseUser user2 = new TBaseUser();
        try {
            user2 = JsonUtil.json2Object(redisUtil.get(token).toString(), TBaseUser.class);
        } catch (Exception e) {
            log.error("json解析异常", e);
        }
//        判断是否是登录状态
        if (user2.getId() == null) {
            log.error("用户未登录");
//            抛出 用户未登录异常
            throw new RbException(ExceptionEnum.NOT_LOGIN);
        }
        TBaseUser tBaseUser = new TBaseUser();
        tBaseUser.setId(id);
        TBaseUser user = tBaseUserMapper.selectByPrimaryKey(tBaseUser);
//        判断用户状态是否有效（1为有效，0为无效）
        if (user.getEffectiveState() == 0) {

//              抛出异常  用户不存在
            throw new RbException(ExceptionEnum.NOT_USER);
        }
//        将EffectiveState变为0  逻辑删除
        user.setEffectiveState(0);
        user.setModifyUserId(user2.getId());
//        更新数据
        int count = tBaseUserMapper.updateByPrimaryKeySelective(user);
//        判断count是否为1
        if (count != 1) {
            log.error("删除用户失败");
//            抛出异常  删除失败
            throw new RbException(ExceptionEnum.DELETE_USER_ERROR);
        }
    }

    /**
     * 更改用户信息
     *
     * @param user
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateUser(TBaseUser user) {
//        新建对象
        TBaseUser user3 = new TBaseUser();
        try {
            user3 = JsonUtil.json2Object(redisUtil.get(user.getToken()).toString(), TBaseUser.class);
        } catch (Exception e) {
            log.error("json解析异常", e);
        }
//        判断是否是登录状态
        if (user3.getId() == null) {
            log.error("用户未登录");
//           抛出异常  用户未登录
            throw new RbException(ExceptionEnum.NOT_LOGIN);
        }
//            判断登录名和id是否存在
        if (tBaseUserMapper.queryUserByLoginName(user.getLoginName()) != null && !tBaseUserMapper.queryUserByLoginName(user.getLoginName()).get("id").equals(user.getId())){
//             抛出异常  此用户名已经存在
            throw new RbException(ExceptionEnum.COMMON_USER_NAME);
        }
//            判断邮箱是否已经存在
        if(tBaseUserMapper.queryUserByEmail(user.getEmail()) != null && !tBaseUserMapper.queryUserByEmail(user.getEmail()).getId().equals(user.getId())){
//          抛出异常邮箱已经存在
            throw new RbException(ExceptionEnum.COMMON_USER_EMAIL);
        }
        user.setModifyUserId(user3.getId());
        int count = tBaseUserMapper.updateByPrimaryKeySelective(user);
//        判断是否更新了数据
        if(count != 1){
            log.error("修改用户失败");
//            抛出异常  修改用户失败
            throw new RbException(ExceptionEnum.UPDATE_USER_ERROR);
        }
    }

    /**
     * 安全退出
     * @param token
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void loginOut(String token) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        TBaseUser user4 = new TBaseUser();
        try{
            user4 = JsonUtil.json2Object(redisUtil.get(token).toString(), TBaseUser.class);
        }catch(Exception e){
            log.error("json解析异常");
        }
//        判断是否是登录状态
        if(user4.getId() == null){
            log.error("用户未登录");
//            抛出异常  用户未登录
            throw new RbException(ExceptionEnum.NOT_LOGIN);
        }
//        添加日志退出信息
        TBaseLoginInfo tBaseLoginInfo = new TBaseLoginInfo();
        TBaseUser user = tBaseUserMapper.queryUserById(user4.getId());
        tBaseLoginInfo.setLogContent(ExceptionEnum.LOGIN_SUCCESS.getMsg());
        tBaseLoginInfo.setStatusType(1);
        tBaseLoginInfo.setUserId(user.getId());
        tBaseLoginInfo.setUserName(user.getUserName());
        tBaseLoginInfo.setModuleId(2);
        tBaseLoginInfo.setOperateIp(user.getLastLoginIp());
        tBaseLoginInfo.setOperateMac(user.getLastLoginMac());
        tBaseLoginInfo.setOperateTime(new Date());
        tBaseLoginInfo.setLogType(15);
        tBaseLoginInfo.setLogContent(ExceptionEnum.OUT_SUCCESS.getMsg());
        tBaseLoginInfo.setActionMethod(request.getServletPath());
        int count = tBaseLoginInfoMapper.insert(tBaseLoginInfo);
        if (count != 1){
//            抛出异常   安全退出失败
            throw new RbException(ExceptionEnum.OUT_LOGIN_ERROR);
        }
//        清楚redis里的token值
        redisUtil.remove(token);
    }

    /**
     * 查找用户列表
     * @param keyWord
     * @param roleId
     * @param userStatus
     * @param page
     * @param rows
     * @return
     */

    @Override
    public PageResult<TBaseUser> queryUserList(String keyWord, Integer roleId, Integer userStatus, Integer page, Integer rows) {
//       创建分页  传入当前页 每页显示多少条
        PageHelper.startPage(page, rows);
//        过滤
        Example example = new Example(TBaseUser.class);
        Example.Criteria criteria = example.createCriteria();
//     判断关键词是否为空
        if(StringUtil.isNotEmpty(keyWord)){
//            过滤条件
            criteria.orLike("loginName", "%" + keyWord + "%").orLike("userName", "%" + keyWord + "%");
        }
//        判断角色id是否为空
        if(roleId != null){
            criteria.andEqualTo("roueId", roleId);
        }

//        判断用户状态是否为空
        if(userStatus != null){
            criteria.andEqualTo("userStatus", userStatus);
        }
//         增加用户时  存在过滤
        criteria.andEqualTo("effectiveState", 1);
//        查询用户列表
        List<TBaseUser> list = tBaseUserMapper.selectByExample(example);
//        判断用户列表是否为空
        if(CollectionUtils.isEmpty(list)){
            log.error("用户列表为空");
//            抛出异常 用户列表为空
            throw new RbException(ExceptionEnum.USER_NOT_FOUND);
        }
//        分页
        PageInfo<TBaseUser> info = new PageInfo<>(list);

        return new PageResult<>(info.getTotal(), info.getPageNum(), list);
    }

    /**
     * 修改密码
     * @param token
     * @param oldPassword
     * @param newPassword
     */
    @Override
    public void updatePassword(String token, String oldPassword, String newPassword) {
        TBaseUser user1 = new TBaseUser();
        try {
            user1 = JsonUtil.json2Object(redisUtil.get(token).toString(), TBaseUser.class);
        }catch(Exception e){
            log.error("json解析失败");
        }
//        判断是否是登录状态
        if(user1.getId() == null){
//            抛出异常 用户未登录
            log.error("用户未登录");
            throw new RbException(ExceptionEnum.NOT_LOGIN);
        }
//        判断用户名是否存在
        TBaseUser user = new TBaseUser();
//        将token的id传给user
        user.setId(user1.getId());
//        通过user的id查找数据库  返回主键
        TBaseUser user2 = tBaseUserMapper.selectByPrimaryKey(user);
//        判断主键是否为空
        if(user2 == null){
            log.error("用户名不存在");
//            抛出异常 用户不存在
            throw new RbException(ExceptionEnum.NOT_USER_NAME);
        }
//        判断密码是否正确
        if(!user2.getPassword().equals(oldPassword)){
//            抛出异常密码错误
            throw new RbException(ExceptionEnum.NOT_USER_NAME);
        }
        user2.setPassword(newPassword);
        user2.setModifyUserId(user1.getId());
//        判断count值是否为1
        int count = tBaseUserMapper.updateByPrimaryKeySelective(user2);
        if(count != 1){
            log.error("更改密码失败");
            throw new RbException(ExceptionEnum.UPDATE_PASSWORD_ERROR);
        }
    }

}